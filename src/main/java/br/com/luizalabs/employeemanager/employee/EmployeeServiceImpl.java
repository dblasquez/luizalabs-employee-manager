package br.com.luizalabs.employeemanager.employee;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl implements EmployeeService{

	@Autowired
	private EmployeeRepository repository;
	
	@Override
	public Employee saveOrUpdate(Employee employee) {
		return repository.save(employee);
	}

	@Override
	public List<Employee> getAll() {
		return repository.findAll();
	}

	@Override
	public Employee getById(Long id) {
		Employee employee = null;
		Optional<Employee> oe = repository.findById(id);
		if(oe.isPresent()) {
			employee = oe.get();
		}
		return employee;
	}

	@Override
	public void delete(Employee employee) {
		repository.delete(employee);
		
	}

}
