package br.com.luizalabs.employeemanager.employee;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.luizalabs.employeemanager.error.EmployeeNotFoundException;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService service;
	
	@GetMapping("/")
	public List<Employee> getAllEmployess() {
		return service.getAll();
	}
	
	@PostMapping("/add")
	public Employee saveEmployee(@Valid @RequestBody Employee employee) {
		return service.saveOrUpdate(employee);
	}
	
	@GetMapping("/{id}")
	public Employee getEmployeeById(@PathVariable Long id) {
		return service.getById(id);
	}
	
	@PostMapping("/update/{id}")
	public Employee updateEmployee(@PathVariable Long id, @Valid @RequestBody Employee employee) {
		Employee employeeEdited = service.getById(id);
		if(employeeEdited == null) {
			throw new EmployeeNotFoundException(id);
		}
		employeeEdited.setName(employee.getName());
		employeeEdited.setEmail(employee.getEmail());
		employeeEdited.setDepartment(employee.getDepartment());
		return service.saveOrUpdate(employeeEdited);
	}
	
	@GetMapping("/delete/{id}")
	public String deleteEmployeeById(@PathVariable Long id) {
		Employee employee = service.getById(id);
		String result = null;
		if(employee != null) {
			service.delete(employee);
			result = "Employee id " + id + " was deleted.";
		}else {
			throw new EmployeeNotFoundException(id);
		}
		return result;
	}
	
}
