package br.com.luizalabs.employeemanager.employee;

import java.util.List;

public interface EmployeeService {

	List<Employee> getAll();
	
	Employee getById(Long id);
	
	Employee saveOrUpdate(Employee employee);
	
	void delete(Employee employee);
	
	
	
	
	
	
	
}
