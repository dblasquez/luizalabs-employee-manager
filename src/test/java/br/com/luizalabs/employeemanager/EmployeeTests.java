package br.com.luizalabs.employeemanager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.luizalabs.employeemanager.employee.Employee;
import br.com.luizalabs.employeemanager.employee.EmployeeService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeTests {

	@Autowired
	private EmployeeService employeeService;
	
	@Test
	public void testSave() {
		Employee employee = new Employee();
		employee.setName("Mr. Test");
		employee.setEmail("mrtest@test.com");
		employee.setDepartment("tester");
		employeeService.saveOrUpdate(employee);
		
		assertNotNull(employee.getId());
		
		employeeService.delete(employee);
	}
	
	@Test
	public void testGetAll() {
				
		Employee daniel = new Employee();
		daniel.setName("Mr. Daniel");
		daniel.setEmail("daniel@test.com");
		daniel.setDepartment("tester");
		employeeService.saveOrUpdate(daniel);
		
		Employee anne = new Employee();
		anne.setName("Mrs. Anne");
		anne.setEmail("mrsanne@test.com");
		anne.setDepartment("developer");
		employeeService.saveOrUpdate(anne);
		
		List<Employee> employees = employeeService.getAll();
		
		assertFalse(employees.isEmpty());
		
		employeeService.delete(daniel);
		employeeService.delete(anne);
		
	}
	
	@Test
	public void testGetById() {
		Employee employee = new Employee();
		employee.setName("Mr. Test");
		employee.setEmail("mrtest@test.com");
		employee.setDepartment("tester");
		employeeService.saveOrUpdate(employee);
		
		Employee employeeTest = employeeService.getById(employee.getId());
		
		assertNotNull(employeeTest);
		
		employeeService.delete(employee);
	}
	
	@Test
	public void testUpdate() {
		Employee employee = new Employee();
		employee.setName("Mr. Test");
		employee.setEmail("mrtest@test.com");
		employee.setDepartment("tester");
		employeeService.saveOrUpdate(employee);
		
		Employee editedEmployee = employeeService.getById(employee.getId());
		editedEmployee.setName("Mr. Test edited");
		employeeService.saveOrUpdate(editedEmployee);
		
		assertEquals("Mr. Test edited", editedEmployee.getName());
		
		employeeService.delete(employee);
	}
	
	@Test
	public void testDelete() {
		Employee employee = new Employee();
		employee.setName("Mr. Test");
		employee.setEmail("mrtest@test.com");
		employee.setDepartment("tester");
		employeeService.saveOrUpdate(employee);
		
		Employee employeeTest = employeeService.getById(employee.getId());
		
		employeeService.delete(employeeTest);
		
		Employee deletedEmployee = employeeService.getById(employee.getId());
		
		assertNull(deletedEmployee);
		
	}

}
